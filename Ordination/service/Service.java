package service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class Service {
	private Storage storage;
	private static Service service;

	private Service() {
		storage = new Storage();
	}

	public static Service getService() {
		if (service == null) {
			service = new Service();
		}
		return service;
	}

	public static Service getTestService() {
		return new Service();
	}

	/**
	 * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
	 * ordinationen oprettes ikke
	 *
	 * @return opretter og returnerer en PN ordination.
	 */
	public PN opretPNOrdination(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			double antal) {
		if (!startDen.isBefore(slutDen)) {
			throw new IllegalArgumentException("Ugyldige datoer");
		} else if (antal <= 0) {
			throw new IllegalArgumentException("Ugyldigt antal");
		}
		PN toReturn = new PN(startDen, slutDen, antal);
		toReturn.setLaegemiddel(laegemiddel);
		patient.addOrdination(toReturn);
		return toReturn;

	}

	/**
	 * Opretter og returnerer en DagligFast ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 */
	public DagligFast opretDagligFastOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
		if (!startDen.isBefore(slutDen)) {
			throw new IllegalArgumentException("Ugyldige datoer");
		} else if (morgenAntal < 0 || middagAntal < 0 || aftenAntal < 0 || natAntal < 0) {
			throw new IllegalArgumentException("Ugyldige antal");
		}
		DagligFast ds = new DagligFast(startDen, slutDen, morgenAntal, middagAntal, aftenAntal, natAntal);
		ds.setLaegemiddel(laegemiddel);
		patient.addOrdination(ds);
		storage.addPatient(patient);

		return ds;
	}

	/**
	 * Opretter og returnerer en DagligSkæv ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 */
	public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, LocalTime[] klokkeSlet, double[] antalEnheder) {
		if (!startDen.isBefore(slutDen)) {
			throw new IllegalArgumentException("Ugyldige datoer");
		} else if (klokkeSlet.length != antalEnheder.length) {
			throw new IllegalArgumentException("Ikke lige mange antal klokkeslet og enheder");
		}
		for (double d : antalEnheder) {
			if (d < 0) {
				throw new IllegalArgumentException("Ugyldige antal enheder");
			}
		}
		DagligSkaev ds = new DagligSkaev(startDen, slutDen);
		ds.setLaegemiddel(laegemiddel);
		patient.addOrdination(ds);
		for (int i = 0; i < klokkeSlet.length; i++) {
			ds.opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
		return ds;
	}

	/**
	 * En dato for hvornår ordinationen anvendes tilføjes ordinationen. Hvis
	 * datoen ikke er indenfor ordinationens gyldighedsperiode kastes en
	 * IllegalArgumentException
	 */
	public void ordinationPNAnvendt(PN ordination, LocalDate dato) {
		if (!ordination.givDosis(dato)) {
			throw new RuntimeException("Datoen er ikke inden for ordinationens gyldighedsperiode");
		}
	}

	/**
	 * Den anbefalede dosis for den pågældende patient (der skal tages hensyn
	 * til patientens vægt). Det er en forskellig enheds faktor der skal
	 * anvendes, og den er afhængig af patientens vægt.
	 */
	public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
		double result;
		if (patient.getVaegt() < 25) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
		} else if (patient.getVaegt() > 120) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
		} else {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
		}
		return result;
	}

	/**
	 * For et givent vægtinterval og et givent lægemiddel, hentes antallet af
	 * ordinationer.
	 */
	public int antalOrdinationerPrVægtPrLægemiddel(double vægtStart, double vægtSlut, Laegemiddel laegemiddel) {
		if (vægtStart > vægtSlut) {
			throw new IllegalArgumentException("Start vægten er højere end slut vægten");
		} else if (vægtStart < 0) {
			throw new IllegalArgumentException("Start vægten skal være positivt");
		}
		int counter = 0;
		List<Patient> patienter = getAllPatienter();
		for (Patient p : patienter) {
			if (vægtStart <= p.getVaegt() && p.getVaegt() <= vægtSlut) {
				ArrayList<Ordination> or = p.getOrdinationer();
				for (Ordination o : or) {
					if (o.getLaegemiddel().equals(laegemiddel)) {
						counter++;
					}
				}
			}
		}

		return counter;
	}

	public List<Patient> getAllPatienter() {
		return storage.getAllPatienter();
	}

	public List<Laegemiddel> getAllLaegemidler() {
		return storage.getAllLaegemidler();
	}

	/**
	 * Metode der kan bruges til at checke at en startDato ligger før en
	 * slutDato.
	 *
	 * @return true hvis startDato er før slutDato, false ellers.
	 */
	private boolean checkStartFoerSlut(LocalDate startDato, LocalDate slutDato) {
		boolean result = true;
		if (slutDato.compareTo(startDato) < 0) {
			result = false;
		}
		return result;
	}

	public Patient opretPatient(String navn, String cpr, double vaegt) {
		Patient p = new Patient(navn, cpr, vaegt);
		storage.addPatient(p);
		return p;
	}

	public Laegemiddel opretLaegemiddel(String navn, double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
			double enhedPrKgPrDoegnTung, String enhed) {
		Laegemiddel lm = new Laegemiddel(navn, enhedPrKgPrDoegnLet, enhedPrKgPrDoegnNormal, enhedPrKgPrDoegnTung,
				enhed);
		storage.addLaegemiddel(lm);
		return lm;
	}

	public void createSomeObjects() {
		opretPatient("Jane Jensen", "121256-0512", 63.4);
		opretPatient("Finn Madsen", "070985-1153", 83.2);
		opretPatient("Hans Jørgensen", "050972-1233", 89.4);
		opretPatient("Ulla Nielsen", "011064-1522", 59.9);
		opretPatient("Ib Hansen", "090149-2529", 87.7);

		opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(0), 3);

		opretPNOrdination(LocalDate.of(2015, 1, 20), LocalDate.of(2015, 1, 25), storage.getAllPatienter().get(3),
				storage.getAllLaegemidler().get(2), 5);

		opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), storage.getAllPatienter().get(0),
				storage.getAllLaegemidler().get(1), 123);

		opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 2, 1, 1, 1);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an);
	}

}
