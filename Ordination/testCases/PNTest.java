package testCases;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class PNTest {

	private Service service;
	private Laegemiddel fucidin;
	private Patient ib;

	@Before
	public void setUp() {
		service = Service.getTestService();
		fucidin = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "styk");
		ib = service.opretPatient("Ib Hansen", "090149-2529", 87.7);

	}

	@Test
	public void testSamletDosisNormal() {
		PN pn1 = service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), ib, fucidin, 123);
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 3));
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 6));
		double result = pn1.samletDosis();

		assertEquals(246, result, 0.01);
	}

	@Test
	public void testSamletDosisGraensevaerdier() {
		PN pn1 = service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), ib, fucidin, 123);
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 1));
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 12));
		double result = pn1.samletDosis();

		assertEquals(246, result, 0.01);
	}

	@Test
	public void testSamletDosisSammeDag() {
		PN pn1 = service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), ib, fucidin, 123);
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 1));
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 1));
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 1));
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 2));
		double result = pn1.samletDosis();

		assertEquals(492, result, 0.01);
	}

	@Test
	public void testDoegnDosisNormal() {
		PN pn1 = service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), ib, fucidin, 123);
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 3));
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 6));
		double result = pn1.doegnDosis();

		assertEquals(61.5, result, 0.01);
	}

	@Test
	public void testDoegnDosisGraensevaerdier() {
		PN pn1 = service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), ib, fucidin, 123);
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 1));
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 12));
		double result = pn1.doegnDosis();

		assertEquals(20.5, result, 0.01);
	}

	@Test
	public void testDoegnDosisSammeDag() {
		PN pn1 = service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), ib, fucidin, 123);
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 1));
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 1));
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 1));
		service.ordinationPNAnvendt(pn1, LocalDate.of(2015, 1, 2));
		double result = pn1.doegnDosis();

		assertEquals(246, result, 0.01);
	}

}