package testCases;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;
import service.Service;

public class DagligFastTest {
	private Service service;
	private Laegemiddel paracetamol;
	private Patient bo;

	@Before
	public void setUp() {
		service = Service.getTestService();
		paracetamol = service.opretLaegemiddel("Fucidin", 1, 1.5, 2, "ml");
		bo = service.opretPatient("Bo Hansen", "090149-1578", 143);
	}

	@Test
	public void testDoegnDosis() {
		DagligFast fast1 = service.opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12), bo,
				paracetamol, 2, 1, 0, 1);
		double result = fast1.doegnDosis();

		assertEquals(4, result, 0.01);
	}

	@Test
	public void testSamletDosis() {
		DagligFast fast1 = service.opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12), bo,
				paracetamol, 2, 1, 0, 1);
		double result = fast1.samletDosis();

		assertEquals(12, result, 0.01);
	}

}