package testCases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class ServiceTest {

	private Service service;
	private Laegemiddel fucidin;
	private Laegemiddel paracetamol;
	private Patient ib;
	private Patient bo;
	private Patient grethe;

	@Before
	public void setup() {
		service = Service.getService();
	}

	public void createObjects() {
		ib = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		bo = service.opretPatient("Bo Hansen", "090149-1578", 143);
		grethe = service.opretPatient("Grethe Hansen", "090149-2527", 20);
		fucidin = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		paracetamol = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), ib, fucidin, 123);
		service.opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14), bo, paracetamol, 3);
		service.opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14), grethe, fucidin, 3);
		service.opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), grethe, paracetamol, 123);
		double[] antal = { 0.5, 1, 2.5, 3 };
		LocalTime[] klokkeslaet = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0),
				LocalTime.of(18, 45) };
		service.opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24), ib, fucidin,
				klokkeslaet, antal);

		service.opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12), bo, paracetamol, 2, 1,
				0, 1);
		service.opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12), bo, fucidin, 2, 1, 0,
				1);
	}

	@Test
	public void testcase01_1() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		PN PN = service.opretPNOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1, 4);

		if (PN == null) {
			fail("PN er ikke oprettet");
		}
	}

	@Test
	public void testcase01_2() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		PN PN = service.opretPNOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1, 4);

		if (!PN.getLaegemiddel().equals(l1)) {
			fail("Lægemiddel er ikke tilføjet PN");
		}
	}

	@Test
	public void testcase01_3() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		PN PN = service.opretPNOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1, 4);

		if (!p1.getOrdinationer().contains(PN)) {
			fail("PN er ikke tilføjet patienten");
		}
	}

	@Test
	public void testcase02() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		try {
			PN PN = service.opretPNOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 2, 6), p1, l1, 4);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ugyldige datoer");
		}
	}

	@Test
	public void testcase03() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		try {
			PN PN = service.opretPNOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1, 0);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ugyldigt antal");
		}
	}

	@Test
	public void testcase04_1() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		DagligFast fast = service.opretDagligFastOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1,
				2, 1, 0, 1);

		if (fast == null) {
			fail("Fast ikke oprettet");
		}
	}

	@Test
	public void testcase04_2() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		DagligFast fast = service.opretDagligFastOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1,
				2, 1, 0, 1);

		if (!fast.getLaegemiddel().equals(l1)) {
			fail("Lægemiddel er ikke tilføjet fast");
		}
	}

	@Test
	public void testcase04_3() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		DagligFast fast = service.opretDagligFastOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1,
				2, 1, 0, 1);

		if (!p1.getOrdinationer().contains(fast)) {
			fail("Fast er ikke tilføjet patienten");
		}
	}

	@Test
	public void testcase04_4() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		DagligFast fast = service.opretDagligFastOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1,
				2, 1, 0, 1);

		if (fast.getDoser()[0] == null) {
			fail("Morgen dosis er ikke oprettet");
		} else if (fast.getDoser()[1] == null) {
			fail("Middag dosis er ikke oprettet");
		} else if (fast.getDoser()[2] == null) {
			fail("Aften dosis er ikke oprettet");
		} else if (fast.getDoser()[3] == null) {
			fail("Nat dosis er ikke oprettet");
		}
	}

	@Test
	public void testcase05() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		try {
			DagligFast fast = service.opretDagligFastOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 2, 6), p1,
					l1, 2, 1, 0, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ugyldige datoer");
		}
	}

	@Test
	public void testcase06() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		try {
			DagligFast fast = service.opretDagligFastOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1,
					l1, -2, 1, 0, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ugyldige antal");
		}
	}

	@Test
	public void testcase07() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		try {
			DagligFast fast = service.opretDagligFastOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1,
					l1, 2, -1, 0, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ugyldige antal");
		}
	}

	@Test
	public void testcase08() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		try {
			DagligFast fast = service.opretDagligFastOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1,
					l1, 2, 1, -1, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ugyldige antal");
		}
	}

	@Test
	public void testcase09() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

		try {
			DagligFast fast = service.opretDagligFastOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1,
					l1, 2, 1, 0, -1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ugyldige antal");
		}
	}

	@Test
	public void testcase10_1() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeslet = { LocalTime.of(8, 30), LocalTime.of(8, 30), LocalTime.of(18, 0) };
		double[] enheder = { 2, 1, 3 };
		DagligSkaev skaev = service.opretDagligSkaevOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1,
				l1, klokkeslet, enheder);

		if (skaev == null) {
			fail("skaev er ikke oprettet");
		}
	}

	@Test
	public void testcase10_2() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeslet = { LocalTime.of(8, 30), LocalTime.of(8, 30), LocalTime.of(18, 0) };
		double[] enheder = { 2, 1, 3 };
		DagligSkaev skaev = service.opretDagligSkaevOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1,
				l1, klokkeslet, enheder);

		if (!skaev.getLaegemiddel().equals(l1)) {
			fail("Laegemiddel ikke tilføjet skaev");
		}
	}

	@Test
	public void testcase10_3() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeslet = { LocalTime.of(8, 30), LocalTime.of(8, 30), LocalTime.of(18, 0) };
		double[] enheder = { 2, 1, 3 };
		DagligSkaev skaev = service.opretDagligSkaevOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1,
				l1, klokkeslet, enheder);

		if (!p1.getOrdinationer().contains(skaev)) {
			fail("Skaev er ikke tilføet patienten");
		}
	}

	@Test
	public void testcase10_4() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeslet = { LocalTime.of(8, 30), LocalTime.of(8, 30), LocalTime.of(18, 0) };
		double[] enheder = { 2, 1, 3 };
		DagligSkaev skaev = service.opretDagligSkaevOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1,
				l1, klokkeslet, enheder);

		if (skaev.getDoser().size() != 3) {
			fail("Skaev har ikke 3 doser");
		}
	}

	@Test
	public void testcase11() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeslet = { LocalTime.of(8, 30), LocalTime.of(8, 30), LocalTime.of(18, 0) };
		double[] enheder = { 2, 1, 3 };

		try {
			DagligSkaev skaev = service.opretDagligSkaevOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 2, 6),
					p1, l1, klokkeslet, enheder);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ugyldige datoer");
		}
	}

	@Test
	public void testcase12() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeslet = { LocalTime.of(8, 30), LocalTime.of(8, 30), LocalTime.of(18, 0) };
		double[] enheder = { 2, 1, 3, 4 };

		try {
			DagligSkaev skaev = service.opretDagligSkaevOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6),
					p1, l1, klokkeslet, enheder);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ikke lige mange antal klokkeslet og enheder");
		}
	}

	@Test
	public void testcase13() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		LocalTime[] klokkeslet = { LocalTime.of(8, 30), LocalTime.of(8, 30), LocalTime.of(18, 0) };
		double[] enheder = { -2, 1, 3 };

		try {
			DagligSkaev skaev = service.opretDagligSkaevOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6),
					p1, l1, klokkeslet, enheder);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ugyldige antal enheder");
		}
	}

	@Test
	public void testcase14() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN PN = service.opretPNOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1, 4);

		int before = PN.getAntalGangeGivet();

		service.ordinationPNAnvendt(PN, LocalDate.of(2016, 3, 3));

		if ((before + 1) != PN.getAntalGangeGivet()) {
			fail("Dosis ikke givet");
		}
	}

	@Test
	public void testcase15() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN PN = service.opretPNOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1, 4);

		int before = PN.getAntalGangeGivet();

		service.ordinationPNAnvendt(PN, LocalDate.of(2016, 3, 6));

		if ((before + 1) != PN.getAntalGangeGivet()) {
			fail("Dosis ikke givet");
		}
	}

	@Test(expected = RuntimeException.class)
	public void testcase16() {
		Patient p1 = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l1 = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN PN = service.opretPNOrdination(LocalDate.of(2016, 3, 3), LocalDate.of(2016, 3, 6), p1, l1, 4);

		service.ordinationPNAnvendt(PN, LocalDate.of(2016, 3, 2));
	}

	@Test
	public void testcase17() {
		Patient p = service.opretPatient("Ib Hansen", "090149-2529", 87.7);
		Laegemiddel l = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

		assertEquals(service.anbefaletDosisPrDoegn(p, l), 131.55, 0);
	}

	@Test
	public void testcase18() {
		Patient p = service.opretPatient("Ib Hansen", "090149-2529", 143);
		Laegemiddel l = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

		assertEquals(service.anbefaletDosisPrDoegn(p, l), 286, 0);
	}

	@Test
	public void testcase19() {
		Patient p = service.opretPatient("Ib Hansen", "090149-2529", 20);
		Laegemiddel l = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

		assertEquals(service.anbefaletDosisPrDoegn(p, l), 20, 0);
	}

	@Test
	public void testcase20() {
		Patient p = service.opretPatient("Ib Hansen", "090149-2529", 24);
		Laegemiddel l = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

		assertEquals(service.anbefaletDosisPrDoegn(p, l), 24, 0);
	}

	@Test
	public void testcase21() {
		Patient p = service.opretPatient("Ib Hansen", "090149-2529", 121);
		Laegemiddel l = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

		assertEquals(service.anbefaletDosisPrDoegn(p, l), 242, 0);
	}

	@Test
	public void testcase22() {
		Patient p = service.opretPatient("Ib Hansen", "090149-2529", 25);
		Laegemiddel l = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

		assertEquals(service.anbefaletDosisPrDoegn(p, l), 37.5, 0);
	}

	@Test
	public void testcase23() {
		Patient p = service.opretPatient("Ib Hansen", "090149-2529", 120);
		Laegemiddel l = service.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");

		assertEquals(service.anbefaletDosisPrDoegn(p, l), 180, 0);
	}

	// antal ordinationer pr. vægt pr. lægemiddel
	@Test
	public void testcase24() {
		createObjects();

		int antalOrdinationer = service.antalOrdinationerPrVægtPrLægemiddel(0, 25, paracetamol);
		assertEquals(1, antalOrdinationer);
	}

	@Test
	public void testcase25() {
		createObjects();

		int antalOrdinationer = service.antalOrdinationerPrVægtPrLægemiddel(25, 120, paracetamol);
		assertEquals(0, antalOrdinationer);
	}

	@Test
	public void testcase26() {
		createObjects();

		int antalOrdinationer = service.antalOrdinationerPrVægtPrLægemiddel(120, 160, paracetamol);
		assertEquals(2, antalOrdinationer);
	}

	@Test
	public void testcase27() {
		try {
			service.antalOrdinationerPrVægtPrLægemiddel(-20, 20, paracetamol);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Start vægten skal være positivt", e.getMessage());
		}
	}

	@Test
	public void testcase28() {
		try {
			service.antalOrdinationerPrVægtPrLægemiddel(20, 10, paracetamol);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Start vægten er højere end slut vægten", e.getMessage());
		}
	}

}
