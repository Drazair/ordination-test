package testCases;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Patient;
import service.Service;

public class DagligSkaevTest {

	private Service service;
	private Laegemiddel fucidin;
	private Patient ib;

	@Before
	public void setUp() {
		service = Service.getTestService();
		fucidin = service.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "styk");
		ib = service.opretPatient("Ib Hansen", "090149-2529", 87.7);

	}

	@Test
	public void testDoegnDosis() {
		double[] antal = { 0.5, 1, 2.5, 3 };
		LocalTime[] klokkeslaet = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0),
				LocalTime.of(18, 45) };
		DagligSkaev skaev = service.opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24), ib,
				fucidin, klokkeslaet, antal);
		double result = skaev.doegnDosis();
		assertEquals(3.5, result, 0.01);
	}

	@Test
	public void testSamletDosis() {
		double[] antal = { 0.5, 1, 2.5, 3 };
		LocalTime[] klokkeslaet = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0),
				LocalTime.of(18, 45) };
		DagligSkaev skaev = service.opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24), ib,
				fucidin, klokkeslaet, antal);
		double result = skaev.samletDosis();
		assertEquals(7, result, 0.01);
	}
}