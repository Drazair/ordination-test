package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {
	private double antalEnheder;
	private ArrayList<LocalDate> dageGivet;

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */

	public PN(LocalDate startDato, LocalDate slutDato, double antalEnheder) {
		super(startDato, slutDato);
		this.antalEnheder = antalEnheder;
		dageGivet = new ArrayList<LocalDate>();
	}

	public boolean givDosis(LocalDate givesDen) {
		boolean toReturn = false;
		if (givesDen.isAfter(getStartDen().minusDays(1)) && givesDen.isBefore(getSlutDen().plusDays(1))) {
			toReturn = true;
			dageGivet.add(givesDen);
		}
		return toReturn;
	}

	@Override
	public double doegnDosis() {
		if (dageGivet.size() > 0) {
			long dage = ChronoUnit.DAYS.between(dageGivet.get(0), dageGivet.get(dageGivet.size() - 1)) + 1;
			return samletDosis() / dage;
		} else {
			return 0;
		}
	}

	@Override
	public double samletDosis() {
		return getAntalGangeGivet() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet() {
		return dageGivet.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
