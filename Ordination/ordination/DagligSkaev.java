package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	private ArrayList<Dosis> dosis;

	public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
		super(startDen, slutDen);
		dosis = new ArrayList<>();
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis d = new Dosis(tid, antal);
		dosis.add(d);
	}

	public void deleteDosis(Dosis dosis) {
		this.dosis.remove(dosis);
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<>(dosis);
	}

	@Override
	public double samletDosis() {
		double sum = 0.0;
		for (Dosis d : dosis) {
			sum += d.getAntal();
		}
		return sum;
	}

	@Override
	public double doegnDosis() {
		return samletDosis() / antalDage();
	}

	@Override
	public String getType() {
		return "DagligSkaev";
	}
}
