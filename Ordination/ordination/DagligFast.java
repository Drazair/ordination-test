package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private Dosis[] dosis = new Dosis[4];
	int antal;

	public DagligFast(LocalDate startDato, LocalDate slutDato, double morgenAntal, double middagAntal,
			double aftenAntal, double natAntal) {
		super(startDato, slutDato);
		LocalTime morgen = LocalTime.parse("06:00");
		LocalTime middag = LocalTime.parse("12:00");
		LocalTime aften = LocalTime.parse("18:00");
		LocalTime nat = LocalTime.parse("00:00");
		dosis[0] = new Dosis(morgen, morgenAntal);
		dosis[1] = new Dosis(middag, middagAntal);
		dosis[2] = new Dosis(aften, aftenAntal);
		dosis[3] = new Dosis(nat, natAntal);

	}

	public Dosis[] getDoser() {
		return dosis;
	}

	@Override
	public double samletDosis() {
		double sum = 0;

		for (int i = 0; i < 4; i++) {
			sum += dosis[i].getAntal();
		}

		return sum * antalDage();

	}

	@Override
	public double doegnDosis() {
		double sum = 0;

		for (int i = 0; i < dosis.length; i++) {
			sum += dosis[i].getAntal();
		}

		return sum;
	}

	@Override
	public String getType() {
		return "Daglig fast";
	}
}
